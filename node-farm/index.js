const fs      = require('fs');
const http    = require('http');
const url     = require('url');
const slugify = require('slugify');

const replaceTemp = require('./modules/replace-template');

//Blocking code, synchronous way
// const textIn  = fs.readFileSync('./txt/input.txt', 'utf-8');
// const textOut = `This is what we know about avocado ${textIn}.\n Created on ${Date.now()}`;
// fs.writeFileSync('./txt/output.txt', textOut);
// console.log('file written blocking way');

//Non Blocking, asynchronous way
// fs.readFile('./txt/start.txt', 'utf-8', (err, data1) => {
//     if(err) return console.log('ERROR 😀');
    
//     fs.readFile(`./txt/${data1}.txt`, 'utf-8', (err, data2) => {
//         console.log('data2', data2);
//         fs.readFile(`./txt/append.txt`, 'utf-8', (err, data3) => {
//             console.log('data3', data3);
            
//             fs.writeFile('./txt/final.txt', `${data2}\n${data3}`, 'utf-8', err => {
//                 console.log('file has written non-blocking way');
//             })
//         })
//     })
// })


/////////////////////////////
/// FILES


/////////////////////////////
/// SERVER

const tempOverview = fs.readFileSync(`${__dirname}/templates/template-overview.html`, 'utf-8');
const tempCard     = fs.readFileSync(`${__dirname}/templates/template-card.html`, 'utf-8');
const tempProduct  = fs.readFileSync(`${__dirname}/templates/template-product.html`, 'utf-8');

//run synchronously
const data    = fs.readFileSync(`${__dirname}/dev-data/data.json`, 'utf-8')
const dataObj = JSON.parse(data);

const slugs = dataObj.map( el => slugify(el.productName, { lower: true }) );
console.log('slug', slugs);


const server = http.createServer( (req, res) => {    

    //set a url
    const {query, pathname} = url.parse(req.url, true)

    //Overview page
    if(pathname === '/' || pathname === '/overview'){
        res.writeHead(200, { 'Content-Type' : 'text/html' })
        
        const cardsHtml = dataObj.map( el => replaceTemp(tempCard, el))
        cardsHtml.join(''); //join into string
        const output = tempOverview.replace('{%PRODUCT_CARDS%}', cardsHtml)
        
        
        res.end(output);
        
        //Product Page
    }else if( pathname === '/product'){
        res.writeHead(200, { 'Content-Type' : 'text/html' });        
        const product = dataObj[query.id];
        
        const output  = replaceTemp(tempProduct, product);
        res.end(output);
    
    // API
    } else if( pathname === '/api'){
        res.writeHead(200, { 'Content-Type' : 'application/json' })
        res.end(data) // can not string type
    
    // Not Found
    } else {
        res.writeHead(404, {
            'Content-Type' : 'text/html',
            'my-own-header': 'hello-world'
        })
        res.end('<h1>This is Unknown</h1>')
    }
})
server.listen(8000, '127.0.0.1', () => {
    console.log('listening on ports 8000');
})

